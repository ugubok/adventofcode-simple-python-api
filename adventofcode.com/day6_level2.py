from wrapper import solve


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    result = 0
    groups = []
    group = []

    for line in inp:
        if line == "":
            groups.append(group)
            group = []
            continue

        group.append(line)

    groups.append(group)

    for g in groups:
        all_q = set()

        for line in g:
            for c in line:
                all_q.add(c)

        for line in g:
            for q in tuple(all_q):
                if q not in line:
                    all_q.remove(q)

        result += len(all_q)

    return result


if __name__ == "__main__":
    solve(solution, day=6, level=2)
