from wrapper import solve
import re


def run_prog(instructions):
    acc, ip = 0, 0
    visited = set()

    while True:
        cmd, arg = instructions[ip]

        if ip in visited:
            return None

        visited.add(ip)

        if cmd == "acc":
            acc += arg

        if cmd == "jmp":
            ip += arg - 1

        ip += 1

        if ip == len(instructions):
            return acc


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    instructions = []

    for line in inp:
        instruction, arg = line.split(" ")
        arg = int(arg)
        instructions.append((instruction, arg))

    for i, v in enumerate(instructions):
        cmd, arg = v

        if cmd == "acc":
            continue

        if cmd == "jmp":
            instructions[i] = ("nop", arg)

        if cmd == "nop":
            instructions[i] = ("jmp", arg)

        result = run_prog(instructions)
        instructions[i] = (cmd, arg)

        if result:
            return result


if __name__ == "__main__":
    solve(solution, day=8, level=2)
