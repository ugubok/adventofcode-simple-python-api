from functools import reduce

from wrapper import solve


def check_passport(p):
    fields = (
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
        "cid",
    )

    for k, _ in p.items():
        if k not in fields:
            print(":: Unknown field: {}".format(k))
            return False

    for f in fields:
        if not p.get(f):
            if f != "cid":
                print(":: No field {}".format(f))
                return False

    print("::VALID")
    return True


def solution(inp):
    lines = inp.strip().split("\n")
    result = 0

    passport = {}
    for line in lines:
        print("> " + line)

        if line == "":
            if check_passport(passport):
                result += 1

            passport = {}
            continue

        for param in line.split(" "):
            k, v = param.split(":")
            passport[k] = v

    if check_passport(passport):
        result += 1

    return result


if __name__ == "__main__":
    solve(solution, day=4, level=1)
