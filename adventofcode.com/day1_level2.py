from wrapper import solve
import itertools


def solution(inp):
    numbers = map(int, inp.strip().split("\n"))

    for x, y, z in itertools.combinations(numbers, 3):
        if x + y + z == 2020:
            return x * y * z


solve(solution, day=1, level=2)
