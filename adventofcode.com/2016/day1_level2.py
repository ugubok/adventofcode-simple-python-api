from wrapper import solve

def solution(inp):
    inp = inp[:-1].split(', ')  # Cut '\n' off
    x = y = 0
    facing = 0  # 0 1 2 3 = N W S E
    visited = []

    for move in inp:
        facing += {'L': 1, 'R': -1}[move[0]]
        if facing < 0: facing = 3
        elif facing > 3: facing = 0
        dst = int(move[1:])

        while dst > 0:
            if facing == 0:  # UP
                y += 1
            elif facing == 2:  # DOWN
                y -= 1
            elif facing == 1:  # LEFT
                x -= 1
            elif facing == 3:  # RIGHT
                x += 1

            if (x, y) in visited:
                return abs(x) + abs(y)
            visited.append((x, y))
            dst -= 1

solve(solution, 1, 2)