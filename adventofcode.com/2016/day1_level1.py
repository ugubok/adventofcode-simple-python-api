from wrapper import solve

def solution(inp):
    inp = inp[:-1].split(', ')  # Cut '\n' off
    x = y = 0
    facing = 0  # 0 1 2 3 = N W S E
    for move in inp:
        facing += {'L': 1, 'R': -1}[move[0]]
        if facing < 0: facing = 3
        elif facing > 3: facing = 0
        dst = int(move[1:])
        if facing == 0:  # UP
            y += dst
        elif facing == 2:  # DOWN
            y -= dst
        elif facing == 1:  # LEFT
            x -= dst
        elif facing == 3:  # RIGHT
            x += dst

    return abs(x) + abs(y)

solve(solution, 1, 1)