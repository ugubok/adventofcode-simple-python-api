__author__ = 'Ugubok'

# ЕТО ОБЕРТКА ДЛЯ API
# ПРОСТО ПОДКЛЮЧАЕШЬ ЕЕ, ПИШЕШЬ РЕШЕНИЕ В ФУНКЦИИ, А ДАЛЬШЕ ОНА ВСЕ СДЕЛАЕТ САМА!

from api import Api


def solve(solution, day, level, debug=False):
    """
    Решить задачу
    :param solution: функция решения. Должна принимать Input и возвращать Output
    :param day: Номер задачи (день)
    :param level: Уровень задачи
    :param debug: не взаимодействовать с сайтом, а только показать ввод задачи и вывод решения
    """
    assert callable(solution)  # Убеждаемся что solution это функция
    api = Api()  # Создаем объект api, тут же проверяется наличие ключа сессии
    if not debug:
        api.login()  # Попытка авторизации с этим ключом
        print("Logined as %s [%s]" % (api.username, api.starcount))
    inp = api.get_input(day)  # Получаем входные данные задачи
    out = solution(inp)  # Решаем задачу
    if debug:
        print(out)
    if not debug:
        print("Answer: %s. Sending..." % out)
        result = api.send_answer(out, day, level)  # Отправляем ответ и смотрим что получилось
        print(result)
