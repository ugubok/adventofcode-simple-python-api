from wrapper import solve


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    results = []
    group = set()

    for line in inp:
        if line == "":
            results.append(len(group))
            group = set()
            continue

        for c in line:
            group.add(c)

    results.append(len(group))

    return sum(results)


if __name__ == "__main__":
    solve(solution, day=6, level=1)
