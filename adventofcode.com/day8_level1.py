from wrapper import solve
import re


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    instructions = []

    for line in inp:
        instruction, arg = line.split(" ")
        arg = int(arg)
        instructions.append((instruction, arg))

    acc, ip = 0, 0
    visited = set()

    while True:
        cmd, arg = instructions[ip]

        if ip in visited:
            break

        visited.add(ip)

        if cmd == "acc":
            acc += arg

        if cmd == "jmp":
            ip += arg - 1

        ip += 1

    return acc


if __name__ == "__main__":
    solve(solution, day=8, level=1)
