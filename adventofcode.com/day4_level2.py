from functools import reduce

from wrapper import solve
import re


def check_passport(p):
    fields = (
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
        "cid",
    )

    for k, _ in p.items():
        if k not in fields:
            print(":: Unknown field: {}".format(k))
            return False

    for f in fields:
        if not p.get(f):
            if f != "cid":
                print(":: No field {}".format(f))
                return False
            else:
                print("::VALID")
                return True

        v = p[f]
        if f == "byr":
            if not re.match(r"^\d{4}$", v):
                return False
                print(f":: INVALID {f} '{v}': should be 4 digits")

            if int(v) not in range(1920, 2002 + 1):
                print(f":: INVALID {f} '{v}': not in range(1920, 2002 + 1)")
                return False

        if f == "iyr":
            if not re.match(r"^\d{4}$", v):
                print(f":: INVALID {f} '{v}': should be 4 digits")
                return False
            if int(v) not in range(2010, 2020 + 1):
                print(f":: INVALID {f} '{v}': not in range(2010, 2020 + 1)")
                return False

        if f == "eyr":
            if not re.match(r"^\d{4}$", v):
                print(f":: INVALID {f} '{v}': should be 4 digits")
                return False
            if int(v) not in range(2020, 2030 + 1):
                print(f":: INVALID {f} '{v}': not in range(2020, 2030 + 1)")
                return False

        if f == "hgt":
            if not re.match(r"^\d+(cm|in)$", v):
                print(f":: INVALID {f} '{v}': xxx(cm/in)")
                return False

            height, is_cm = int(v.replace("cm", "").replace("in", "")), "cm" in v

            if is_cm and height not in range(150, 193 + 1):
                print(f":: INVALID {f} '{v}': should be 150 - 193 cm")
                return False

            if not is_cm and height not in range(59, 76 + 1):
                print(f":: INVALID {f} '{v}': should be 59 - 76 in")
                return False

        if f == "hcl":
            if not re.match(r"^#[0-9a-f]{6}$", v):
                print(f":: INVALID {f} '{v}': should be hex color")
                return False

        if f == "ecl":
            if not re.match(r"^(amb|blu|brn|gry|grn|hzl|oth)$", v):
                print(f":: INVALID {f} '{v}': should be amb|blu|brn|gry|grn|hzl|oth")
                return False

        if f == "pid":
            if not re.match(r"^\d{9}$", v):
                print(f":: INVALID {f} '{v}': should be 9 digits")
                return False

    print("::VALID")
    return True


def solution(inp):
    lines = inp.strip().split("\n")
    result = 0

    passport = {}
    for line in lines:
        print("> " + line)

        if line == "":
            if check_passport(passport):
                result += 1

            passport = {}
            continue

        for param in line.split(" "):
            k, v = param.split(":")
            passport[k] = v

    if check_passport(passport):
        result += 1

    return result


if __name__ == "__main__":
    solve(solution, day=4, level=2)
