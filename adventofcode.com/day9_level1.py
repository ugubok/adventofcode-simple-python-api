from wrapper import solve
import itertools


def solution(inp):
    inp = map(int, inp.strip().split("\n"))
    preamble = [next(inp) for _ in range(25)]

    for x in inp:
        sums = set(a + b for a, b in itertools.combinations(preamble, 2))

        if x not in sums:
            return x

        preamble.pop(0)
        preamble.append(x)


if __name__ == "__main__":
    solve(solution, day=9, level=1)
