# ПРОСТОЙ API ДЛЯ adventofcode.com
__author__ = "Ugubok"

from os import path
import requests
import re

SESSION_FILE_NAME = "SESSION"  # Имя файла сессии
URL = "https://adventofcode.com/2020/"
MYDIR = path.dirname(__file__)  # Рабочая директория скрипта
# Регулярное выражение извлечения имени пользователя
username_rexp = re.compile('<div[^>]*class="user"[^>]*>([^<]*)')
# Регулярное выражение извлечения количества звезд
starcount_rexp = re.compile('<span[^>]*class="star-count"[^>]*>([^<]*)')
# Регулярное выражение извлечения результата выполнения задачи (после отправки ответа)
result_rexp = re.compile("<main>(.+?)</main>", re.S)


def cuttags(htmltext):
    """Пытается вырезать html-теги из текста"""
    r = re.compile("<[^>]*>")
    found = r.search(htmltext)
    while found:
        htmltext = htmltext[: found.span()[0]] + htmltext[found.span()[1] :]
        found = r.search(htmltext)
    return htmltext


class Api(object):
    def __init__(self):
        """
        Проверяет наличие файла сессии и считывает из него ключ сессии
        """
        try:
            with open(path.join(MYDIR, SESSION_FILE_NAME), "tr") as f:
                self.sessid = f.read().strip()
        except FileNotFoundError:
            raise Exception(
                """
    Session file not found!
    You must create file "%s" in %s
    Then insert session key into it, which can get from cookies after login on adventofcode.com
    This key allows to login with your name, so don't forget to delete this file before distribution
            """
                % (SESSION_FILE_NAME, MYDIR)
            )

        self.session = requests.Session()
        session_cookie = requests.cookies.create_cookie("session", self.sessid)
        self.session.cookies.set_cookie(session_cookie)

    def login(self):
        """
        Выполняет попытку авторизации на сайте с использованием ключа сессии из файла сессии
        При успехе сохраняет имя пользователя и количество звезд
        """
        r = self.session.get(URL)
        username = username_rexp.findall(r.text)
        starcount = starcount_rexp.findall(r.text)
        if not username:
            raise Exception(
                """
    LOGIN ATTEMPT FAILED
    We try to log in with session key in file %s
    This session key may be expired or invalid
    Try to insert new session key into it, which can get from cookies after login on adventofcode.com
    This key allows to login with your name, so don't forget to delete this file before distribution
            """
                % path.join(MYDIR, SESSION_FILE_NAME)
            )
        self.username = "" if not username else username[0].strip()
        self.starcount = "" if not starcount else starcount[0].strip()

    def get_input(self, day):
        """
        Запрашивает входные данные для задачи
        :param day: номер задачи
        :return: входные данные задачи
        """
        url = "%sday/%d/input" % (URL, day)
        r = self.session.get(url)
        return r.text

    def send_answer(self, answer, day, level):
        """
        Посылает ответ к задаче, возвращает обработанный текст результата
        :param answer: ответ к задаче
        :param day: номер задачи
        :param level: уровень
        :return: обработанный текст результата
        """
        url = "{}day/{}/answer".format(URL, day)
        data = {"level": level, "answer": answer}
        r = self.session.post(url, data)
        result = result_rexp.findall(r.text)

        if len(result) == 1:
            return cuttags(result[0])

        return result or "Can't find result!"
