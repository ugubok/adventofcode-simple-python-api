from wrapper import solve
import re


class Bag(object):
    def __init__(self, name):
        self.name = name
        self._can_contain = []

    def add_contain(self, other):
        if other in self._can_contain:
            return
        self._can_contain.append(other)

    def can_contain(self, other):
        if other in self._can_contain:
            return True
        for inner_bag in self._can_contain:
            if inner_bag.can_contain(other):
                return True
        return False

    def __repr__(self):
        return f"<Bag '{self.name}'>"


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    bags = {}
    needed_bag = "shiny gold"

    for line in inp:
        m = re.match(r"^([\w ]+) bags? contain ([^.]+).?$", line)
        bag_name, contains = m.groups()

        bag = bags.get(bag_name, Bag(bag_name))
        bags[bag_name] = bag

        for bag_rule in contains.split(","):
            n, b = re.match("^\s?(no|\d+) ([\w\s]+) bags?$", bag_rule).groups()

            if n == "no":
                continue

            if b not in bags:
                bags[b] = Bag(b)

            bag.add_contain(bags[b])

    return sum(1 for _, bag in bags.items() if bag.can_contain(bags[needed_bag]))


if __name__ == "__main__":
    solve(solution, day=7, level=1)
