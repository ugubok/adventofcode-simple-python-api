from wrapper import solve
import itertools


def solution(inp):
    numbers = map(int, inp.strip().split("\n"))

    for x, y in itertools.combinations(numbers, 2):
        if x + y == 2020:
            return x * y


solve(solution, day=1, level=1)
