from wrapper import solve


def solution(inp):
    inp = inp.splitlines()
    codelen = 0
    lenraw = 0
    for line in inp:
        codelen += len(line)
        raw = line.replace('\\\\', '////')
        raw = raw.replace(r'\"', r'\\\"')
        raw = r'"\"' + raw[1:-1] + r'\""'
        raw = raw.replace(r'\x', r'\\x')
        lenraw += len(raw)
    return lenraw - codelen
solve(solution, day=8, level=2)