from wrapper import solve


def solution(inp):
    assert isinstance(inp, str)
    lines = inp.splitlines()
    total = 0
    for line in lines:
        l, w, h = map(int, line.split('x'))
        total += 2*l*w + 2*w*h + 2*h*l + min(w*l, w*h, l*h)
    return total


solve(solution, day=2, level=1)
