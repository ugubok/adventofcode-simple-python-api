from wrapper import solve


def solution(inp):
    global cmap
    x = y = 0  # Текущие координаты
    cmap = {0: {0: "@"}}  # Карта
    total = 1
    for c in inp:
        x += {'<': -1, '>': 1}.get(c) or 0
        y += {'v': -1, '^': 1}.get(c) or 0
        if not cmap.get(x):
            cmap[x] = {y: "@"}
            total += 1
        elif not cmap[x].get(y):
            cmap[x][y] = "@"
            total += 1
    # Ето весь код задачи. Дальше код которым я напечатаю карту (ето интересно)
        else:
            if cmap[x][y] == "@":
                cmap[x][y] = 1
            else:
                cmap[x][y] += 1
        global xmax, xmin, ymax, ymin
        if 'xmax' not in globals():
            xmax = xmin = x
            ymax = ymin = y
        if x > xmax:
            xmax = x
        elif x < xmin:
            xmin = x
        if y > ymax:
            ymax = y
        elif y < ymin:
            ymin = y
    return total

solve(solution, day=3, level=1)

# Ненужный код которым я напечатаю карту в файл
f = open('day3_map.txt', 'tw')
for y in range(ymin, ymax):
    for x in range(xmin, xmax):
        if cmap.get(x):
            if cmap[x].get(y):
                f.write(str(cmap[x][y]))
            else:
                f.write(' ')
        else:
            f.write(' ')
    f.write('\n')
f.close()

# Код ниже не нужен - он рисует матрицу в файл
from struct import pack
from binascii import crc32
from zlib import adler32


class PNGImageLow(object):
    """Работа с PNG на низком уровне"""
    PNG_SIGNATURE = bytearray(b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a')  # Заголовок PNG

    @staticmethod
    def makechunk(chtype, data):
        """
        Создает PNG-чанк
        :param chtype: тип чанка (4 символа, строка или байты)
        :param data: данные чанка (байты)
        :return: правильный PNG-чанк
        """
        if type(chtype) is str:
            chtype = bytearray(chtype, 'ascii')
        return pack('>L', len(data)) + chtype + data + pack('>L', crc32(chtype + data))

    @staticmethod
    def make_ihdr_chunk(width, height, bitdepth, colortype, compression=0, filtermethod=0, interlacemethod=0):
        """
        Создает чанк заголовка изображения (IHDR)
        :param width: Ширина изображения
        :param height: Высота изображения
        :param bitdepth: Битовая глубина, бит на сэмпл
        :param colortype: Тип цвета
        :param compression: Метод сжатия
        :param filtermethod: Метод фильтрации
        :param interlacemethod: Метод переплетения
        :return: чанк заголовка изображения (IHDR)
        """
        return PNGImageLow.makechunk('IHDR',
                                     pack('>LLBBBBB', width, height, bitdepth, colortype, compression,
                                          filtermethod, interlacemethod))

    @staticmethod
    def make_plte_chunk(*args):
        """
        Создает чанк палитры (PLTE)
        :param args: кортежи типа (R, G, B) или строка байт с длиной кратной 3
        :return: чанк палитры PLTE с переданными цветами
        """
        if isinstance(args[0], bytes) or isinstance(args[0], bytearray):
            palette = args[0]
            assert len(palette) % 3 == 0  # Кол-во байтов должно быть кратно 3
        else:
            palette = b''
            for r, g, b in args:
                palette += bytes([r, g, b])
        return PNGImageLow.makechunk('PLTE', palette)

    @staticmethod
    def make_iend_chunk():
        """Создает чанк конца PNG (IEND)"""
        return PNGImageLow.makechunk('IEND', b'')

png_height = ymax - ymin
png_width = xmax - xmin

matrix = [[0 for xi in range(png_width)] for x in range(png_height)]
for y in range(ymin, ymax):
    for x in range(xmin, xmax):
        if cmap.get(x):
            if cmap[x].get(y):
                if cmap[x][y] == "@":
                    matrix[y][x] = 1
                else:
                    matrix[y][x] = 1 + cmap[x][y]
                    if(matrix[y][x] > 11):
                        matrix[y][x] = 12


png_ihdr = PNGImageLow.make_ihdr_chunk(png_width, png_height, bitdepth=8, colortype=3)  # 3 - USE PALETTE
png_plte = PNGImageLow.make_plte_chunk(
    (0, 0, 0),
    (255, 0, 0),
    (255, 30, 0),
    (255, 50, 0),
    (255, 80, 0),
    (255, 110, 0),
    (255, 150, 0),
    (255, 180, 0),
    (255, 210, 0),
    (255, 255, 0),
    (255, 255, 30),
    (255, 255, 60),
    (255, 0, 255)
)
png_iend = PNGImageLow.make_iend_chunk()
ZH = b'\x78\x01'  # zlib header
# Еще одна ошибка была в том, что в заголовке DEFLATE паковать нужно в Little-Endian
DH = b'\x00' + pack('<HH', png_width+1, 0xffff - (png_width+1))  # DEFLATE header / width+1 - 1 байт на режим фильтрации
alldata = b''  # Для вычисления adler32

f = open('day3_map.png', 'bw')
f.write(PNGImageLow.PNG_SIGNATURE)  # Сначала пишем сигнатуру PNG
f.write(png_ihdr)  # Заголовочный чанк
f.write(png_plte)  # Чанк с палитрой
# Пишем первый чанк IDAT
# Ошибка была в том, что первый байт после заголовка deflate это некий filter type (должен быть 0)
# Я же писал туда строку пикселей без этого байта, и все смещалось
pixels_row = b'\x00' + bytes([light for light in matrix[0]])
alldata += pixels_row
f.write(PNGImageLow.makechunk('IDAT', ZH + DH + pixels_row))
# Пишем последующие чанки
for pixels_row in matrix[1:-1]:
    pixels_row = b'\x00' + bytes([light for light in pixels_row])
    f.write(PNGImageLow.makechunk('IDAT', DH + pixels_row))
    alldata += pixels_row
# Пишем последний блок
pixels_row = b'\x00' + bytes([light for light in matrix[-1]])
alldata += pixels_row
adler = pack('>L', adler32(alldata))  # Считаем какой-то хеш adler32 всех данных в deflate
lastchunk = PNGImageLow.makechunk('IDAT', b'\x01' + DH[1:] + pixels_row + adler)
f.write(lastchunk)  # В последнем чанке первый байт заголовка deflate должен быть 1
f.write(png_iend)
f.close()
