from wrapper import solve


def solution(inp):
    global matrix
    inp = inp.splitlines()
    total = 0
    matrix = [[False for i in range(1000)] for i in range(1000)]
    turnon = lambda l: True
    turnoff = lambda l: False
    toggle = lambda l: not l
    for line in inp:
        instruction = turnon if 'turn on' in line else turnoff if 'turn off' in line else toggle
        line = line.split(' ')
        if ',' not in line[1]:
            line.pop(1)
        y1, x1, y2, x2 = map(int, line[1].split(',') + line[3].split(','))
        for y in range(y1, y2+1):
            for x in range(x1, x2+1):
                matrix[y][x] = instruction(matrix[y][x])
    for line in matrix:  # Подсчет горящих лампочек
        for light in line:
            total += light
    return total

solve(solution, day=6, level=1)

# Код ниже не нужен - он рисует матрицу в файл
from struct import pack
from binascii import crc32
from zlib import adler32


class PNGImageLow(object):
    """Работа с PNG на низком уровне"""
    PNG_SIGNATURE = bytearray(b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a')  # Заголовок PNG

    @staticmethod
    def makechunk(chtype, data):
        """
        Создает PNG-чанк
        :param chtype: тип чанка (4 символа, строка или байты)
        :param data: данные чанка (байты)
        :return: правильный PNG-чанк
        """
        if type(chtype) is str:
            chtype = bytearray(chtype, 'ascii')
        return pack('>L', len(data)) + chtype + data + pack('>L', crc32(chtype + data))

    @staticmethod
    def make_ihdr_chunk(width, height, bitdepth, colortype, compression=0, filtermethod=0, interlacemethod=0):
        """
        Создает чанк заголовка изображения (IHDR)
        :param width: Ширина изображения
        :param height: Высота изображения
        :param bitdepth: Битовая глубина, бит на сэмпл
        :param colortype: Тип цвета
        :param compression: Метод сжатия
        :param filtermethod: Метод фильтрации
        :param interlacemethod: Метод переплетения
        :return: чанк заголовка изображения (IHDR)
        """
        return PNGImageLow.makechunk('IHDR',
                                     pack('>LLBBBBB', width, height, bitdepth, colortype, compression,
                                          filtermethod, interlacemethod))

    @staticmethod
    def make_plte_chunk(*args):
        """
        Создает чанк палитры (PLTE)
        :param args: кортежи типа (R, G, B) или строка байт с длиной кратной 3
        :return: чанк палитры PLTE с переданными цветами
        """
        if isinstance(args[0], bytes) or isinstance(args[0], bytearray):
            palette = args[0]
            assert len(palette) % 3 == 0  # Кол-во байтов должно быть кратно 3
        else:
            palette = b''
            for r, g, b in args:
                palette += bytes([r, g, b])
        return PNGImageLow.makechunk('PLTE', palette)

    @staticmethod
    def make_iend_chunk():
        """Создает чанк конца PNG (IEND)"""
        return PNGImageLow.makechunk('IEND', b'')

png_height = len(matrix)
png_width = len(matrix[0])

png_ihdr = PNGImageLow.make_ihdr_chunk(png_width, png_height, bitdepth=8, colortype=3)  # 3 - USE PALETTE
png_plte = PNGImageLow.make_plte_chunk((000, 0, 0), (000, 0, 255), (000, 255, 0), (000, 255, 255),
                                       (255, 0, 0), (255, 0, 255), (255, 255, 0), (255, 255, 255))
colors = {
    'black': 0, 'blue': 1, 'green': 2, 'cyan': 3,
    'red': 4, 'pink': 5, 'yellow': 6, 'white': 7
}
png_iend = PNGImageLow.make_iend_chunk()
ZH = b'\x78\x01'  # zlib header
# Еще одна ошибка была в том, что в заголовке DEFLATE паковать нужно в Little-Endian
DH = b'\x00' + pack('<HH', png_width+1, 0xffff - (png_width+1))  # DEFLATE header / width+1 - 1 байт на режим фильтрации
alldata = b''  # Для вычисления adler32

f = open('day6_matrix.png', 'bw')
f.write(PNGImageLow.PNG_SIGNATURE)  # Сначала пишем сигнатуру PNG
f.write(png_ihdr)  # Заголовочный чанк
f.write(png_plte)  # Чанк с палитрой
# Пишем первый чанк IDAT
# Ошибка была в том, что первый байт после заголовка deflate это некий filter type (должен быть 0)
# Я же писал туда строку пикселей без этого байта, и все смещалось
pixels_row = b'\x00' + bytes([colors['red'] * light for light in matrix[0]])
alldata += pixels_row
f.write(PNGImageLow.makechunk('IDAT', ZH + DH + pixels_row))
# Пишем последующие чанки
for pixels_row in matrix[1:-1]:
    pixels_row = b'\x00' + bytes([colors['red'] * light for light in pixels_row])
    f.write(PNGImageLow.makechunk('IDAT', DH + pixels_row))
    alldata += pixels_row
# Пишем последний блок
pixels_row = b'\x00' + bytes([colors['red'] * light for light in matrix[-1]])
alldata += pixels_row
adler = pack('>L', adler32(alldata))  # Считаем какой-то хеш adler32 всех данных в deflate
lastchunk = PNGImageLow.makechunk('IDAT', b'\x01' + DH[1:] + pixels_row + adler)
f.write(lastchunk)  # В последнем чанке первый байт заголовка deflate должен быть 1
f.write(png_iend)
f.close()
