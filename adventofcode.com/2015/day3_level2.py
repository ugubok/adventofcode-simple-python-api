from wrapper import solve


def solution(inp):
    x = y = 0  # Текущие координаты санты
    rx = ry = 0  # Текущие координаты робота
    cmap = {0: {0: "@"}}  # Карта
    total = 1

    def visited(x, y):
        if not cmap.get(x) or not cmap[x].get(y):
            return False
        return True

    def visit(x, y):
        if not cmap.get(x):
            cmap[x] = {y: "@"}
        elif not cmap[x].get(y):
            cmap[x][y] = "@"

    for c, rc in zip(inp[::2], inp[1::2]):
        x += {'<': -1, '>': 1}.get(c) or 0
        y += {'v': -1, '^': 1}.get(c) or 0
        rx += {'<': -1, '>': 1}.get(rc) or 0
        ry += {'v': -1, '^': 1}.get(rc) or 0
        if not visited(x, y):
            visit(x, y)
            total += 1
        if not visited(rx, ry):
            visit(rx, ry)
            total += 1
    return total

solve(solution, day=3, level=2)