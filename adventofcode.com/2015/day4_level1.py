from hashlib import md5

from wrapper import solve


def solution(inp):
    inp = inp.strip()
    i = 1
    m = md5((inp + str(i)).encode()).hexdigest()
    while m[:5] != '00000':
        i += 1
        m = md5((inp + str(i)).encode()).hexdigest()
    print("md5: %s\ni: %d" % (m, i))
    return i

solve(solution, day=4, level=1)
