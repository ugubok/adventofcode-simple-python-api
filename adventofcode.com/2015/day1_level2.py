from wrapper import solve


def solution(inp):
    floor = 0
    i = 0
    for i in range(len(inp)):
        floor += 1 if inp[i] is '(' else -1
        if floor == -1:
            break
    return i+1


solve(solution, day=1, level=2)
