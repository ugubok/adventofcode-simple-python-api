from wrapper import solve


def solution(inp):
    floor = 0
    for c in inp:
        floor += 1 if c is '(' else -1
    return floor

solve(solution, day=1, level=1)
