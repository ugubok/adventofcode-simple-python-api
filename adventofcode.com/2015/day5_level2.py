from wrapper import solve


def solution(inp):
    inp = inp.splitlines()
    total = 0
    for line in inp:
        hasxyx = False
        hasdoubles = False
        i = 0
        for c in line:
            if not hasdoubles and i >= 1 and line[i-1] + c in line[i+1:]:
                hasdoubles = True
                if hasxyx:
                    break
            if not hasxyx and c == line[i+2:i+3] and c != line[i+1:i+2]:
                hasxyx = True
                if hasdoubles:
                    break
            i += 1
        if hasxyx and hasdoubles:
            total += 1
    return total

solve(solution, day=5, level=2)
