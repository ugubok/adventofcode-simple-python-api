from wrapper import solve


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    results = []

    for line in inp:
        row = (0, 127)
        col = (0, 7)

        for c in line:
            if c == "B":
                row[1] //= 2
            elif c == "F":
                row[0] += row[1] // 2
            elif c == "L":
                row[1] //= 2
            elif c == "L":
                row[1] //= 2

        row, col = min(row), min(col)
        results.append(row * 8 + col)

    return max(results)


if __name__ == "__main__":
    solve(solution, day=5, level=1)
