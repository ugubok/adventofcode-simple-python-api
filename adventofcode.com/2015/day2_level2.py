from wrapper import solve


def solution(inp):
    assert isinstance(inp, str)
    lines = inp.splitlines()
    total = 0
    for line in lines:
        l, w, h = map(int, line.split('x'))
        min1 = min(l, w, h)
        min2 = min(l, w) if min1 == h else min(w, h) if min1 == l else min(l, h)
        total += 2*min1 + 2*min2 + l*w*h
    return total


solve(solution, day=2, level=2)
