from wrapper import solve


def solution(inp):
    inp = inp.splitlines()
    codelen = 0
    lenclean = 0
    for line in inp:
        codelen += len(line)
        clean = line[1:-1]
        clean = clean.replace(r'\\', '/')
        clean = clean.replace(r'\"', '"')
        fpos = clean.find(r'\x')
        while fpos != -1:
            clean = clean[:fpos] + '*' + clean[fpos+4:]
            fpos = clean.find(r'\x')
        lenclean += len(clean)
    return codelen - lenclean
solve(solution, day=8, level=1)