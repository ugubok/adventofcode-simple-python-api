from wrapper import solve


"""
    СКОЛЬКО ЧАСОВ Я ПРОЕБАЛСЯ С ЭТОЙ ЗАДАЧКОЙ. ВЫПОЛНЕНИЕ НЕ ПРЕКРАЩАЛОСЬ ОКОЛО МИНУТЫ
    СНАЧАЛА ДУМАЛ ЧТО ПРОГРАММА ЗАЦИКЛИЛАСЬ ГДЕ-ТО В РЕКУРСИВНЫХ ВЫЗОВАХ get_wire_signal
    НАЧАЛ СОСТАВЛЯТЬ И АНАЛИЗИРОВАТЬ ЛОГИ, УВИДЕЛ ТАМ, ЧТО СИГНАЛ КАЖДОГО ПРОВОДА ВЫЧИСЛЯЕТСЯ
    КАЖДЫЙ РАЗ ЗАНОВО. ПРИКРУТИЛ ЗАПОМИНАНИЕ УЖЕ ВЫЧИСЛЕННЫХ СИГНАЛОВ, И ВСЕ СРАЗУ ЖЕ ВЫЧИСЛИЛОСЬ
    ЗА 511 ШАГОВ. ВЫХОДИТ ОШИБКИ НЕ БЫЛО, ПРОСТО ЗАДАЧА СОСТАВЛЕНА ТАК ХИТРО, ЧТО ЕСЛИ НЕ ЗАПОМИНАТЬ
    УЖЕ ВЫЧИСЛЕННОЕ, БУДЕТ ВЫЧИСЛЯТЬ КУЧУ ВРЕМЕНИ
"""


class WireMachine(object):
    class Wire(object):
        def __init__(self, signal=None):
            self._signal = None
            self._connection = None
            if type(signal) is int:
                self.signal = signal

        @property
        def signal(self):
            return self._signal

        @signal.setter
        def signal(self, value):
            assert isinstance(value, int)
            if value >= 0:
                self._signal = value
            else:
                raise ValueError("Signal must be positive number")

        def powered(self):
            return self.signal is not None

        def connect(self, gate):
            assert isinstance(gate, list)
            if len(gate) not in (1, 2, 3):
                raise ValueError("Gate %s seems not valid" % ' '.join(gate))
            if len(gate) == 2 and gate[0] != 'NOT':
                raise ValueError("Gate %s seems not valid" % ' '.join(gate))
            if len(gate) == 3 and gate[1] not in ['AND', 'OR', 'LSHIFT', 'RSHIFT']:
                raise ValueError("Gate %s seems not valid" % ' '.join(gate))
            self._connection = gate

        def connected(self):
            return self.connection is not None

        @property
        def connection(self):
            return self._connection

    def __init__(self):
        self._wires = dict()
        self._knowledge = dict()  # Сюда запомнит уже вычисленные сигналы

    def haswire(self, name) -> bool:
        return name in self._wires.keys()

    def addwire(self, name, value):
        """
        Добавляет провод
        :param name: Имя провода
        :param value: Сигнал либо инструкция по вычислению типа "af AND ah"
        """
        if type(value) is int:
            self._wires[name] = self.Wire(value)
        elif type(value) is str:
            wire = self.Wire()
            wire.connect(value.split(' '))
            self._wires[name] = wire

    def get_wire_signal(self, name, tablvl=0):
        assert isinstance(name, str)
        wire = self._wires.get(name)
        if name.isdigit():
            return int(name)
        if not wire:
            print("None because of no wire with name %s" % name)
            return None
        if name in self._knowledge:
            # print("%s%s = %d (knowledge)" % ('  '*tablvl, name, self._knowledge[name]))
            return self._knowledge[name]
        if not wire.powered() and not wire.connected():
            # print("None because of %s not powered and not connected" % name)
            return None
        if wire.powered():
            # print("%s%s = %d" % ('  '*tablvl, name, wire.signal))
            return wire.signal
        if len(wire.connection) == 1:  # Подключен к другому проводу
            # print("%s%s = %s" % ('  '*tablvl, name, wire.connection[0]))
            result = self.get_wire_signal(wire.connection[0], tablvl+1)
            self._knowledge[wire.connection[0]] = result
            return result
        if len(wire.connection) == 2 and wire.connection[0] == 'NOT':
            # print("%s%s = %s" % ('  '*tablvl, name, ' '.join(wire.connection)))
            signal = self.get_wire_signal(wire.connection[1], tablvl+1)
            self._knowledge[wire.connection[1]] = signal
            if signal is None:
                print("[%s] None because of no signal on connected wire named %s" % (name, wire.connection[1]))
                return None
            return 0b1111111111111111 & ~signal  # КАЖЕЦА ТУТ НУЖНЫ НЕБОЛЬШИЕ ПАЯСНЕНИЯ?
            # В условии было сказано что на переменную 16 бит, поэтому NOT 123 будет 65412
            # В путоне нет никакого контроля за битовой длиной переменных, поэтому ~ не сработает как надо
            # Наложив результат ~ на 16 битов единиц, можно получить нужный результат
        wire1, gate, wire2 = wire.connection
        # print("%s%s = %s" % ('  '*tablvl, name, ' '.join(wire.connection)))
        signal1 = self.get_wire_signal(wire1, tablvl + 1)
        self._knowledge[wire1] = signal1
        signal2 = self.get_wire_signal(wire2, tablvl + 1)
        self._knowledge[wire2] = signal2
        if signal1 is None or signal2 is None:
            print("[%s] None because of no signal on this: %s" % (name, ' '.join(wire.connection)))
            return None
        return {
            'AND': signal1 & signal2, 'OR': signal1 | signal2,
            'LSHIFT': signal1 << signal2, 'RSHIFT': signal1 >> signal2
        }[gate]


def solution(inp):
    inp = inp.splitlines()
    wirestation = WireMachine()
    for line in inp:
        signal, wire = line.split(' -> ')
        if signal.isdigit():
            signal = int(signal)
        wirestation.addwire(wire, signal)
    return wirestation.get_wire_signal('a')
solve(solution, day=7, level=1)
