from wrapper import solve
import re


class Bag(object):
    def __init__(self, name):
        self.name = name
        self._can_contain = {}

    def add_contain(self, other, n):
        if other in self._can_contain:
            return
        self._can_contain[other] = n

    def can_contain(self, other):
        if other in self._can_contain:
            return True
        for _, inner_bag in self._can_contain.items():
            if inner_bag.can_contain(other):
                return True
        return False

    def sum_bags(self):
        result = sum(bag.sum_bags() * n + n for bag, n in self._can_contain.items())
        print(f"{self}: sum_bags = {result}")
        return result

    def __repr__(self):
        return f"<Bag '{self.name}'>"


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    bags = {}
    needed_bag = "shiny gold"

    for line in inp:
        m = re.match(r"^([\w ]+) bags? contain ([^.]+).?$", line)
        bag_name, contains = m.groups()

        bag = bags.get(bag_name, Bag(bag_name))
        bags[bag_name] = bag

        for bag_rule in contains.split(","):
            n, b = re.match("^\s?(no|\d+) ([\w\s]+) bags?$", bag_rule).groups()

            if n == "no":
                continue

            if b not in bags:
                bags[b] = Bag(b)

            bag.add_contain(bags[b], int(n))

    return bags[needed_bag].sum_bags()


if __name__ == "__main__":
    solve(solution, day=7, level=2)
