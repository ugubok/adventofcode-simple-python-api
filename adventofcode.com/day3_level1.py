from wrapper import solve
import itertools


def count_trees(map_, right, down):
    result = 0
    height, width = len(map_), len(map_[0])

    for y in range(1, height, down):
        x = y * right % width

        if map_[y][x] == "#":
            result += 1

    return result


def solution(inp):
    map_ = inp.strip().split("\n")
    return count_trees(map_, right=3, down=1)


solve(solution, day=3, level=1)
