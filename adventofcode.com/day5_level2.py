from wrapper import solve


def solution(inp):
    inp = map(str.strip, inp.strip().split("\n"))
    results = []

    for line in inp:
        row = [0, 127]
        col = [0, 7]
        distance = lambda x: x[1] - x[0]

        for c in line:
            if c == "F":
                row[1] -= distance(row) // 2 + 1
            elif c == "B":
                row[0] += distance(row) // 2 + 1
            elif c == "L":
                col[1] -= distance(col) // 2 + 1
            elif c == "R":
                col[0] += distance(col) // 2 + 1

        row, col = min(row), max(col)
        results.append(row * 8 + col)

    for i in range(min(results), max(results)):
        if i not in results:
            return i


if __name__ == "__main__":
    solve(solution, day=5, level=2)
