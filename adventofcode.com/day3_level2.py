import itertools
from functools import reduce

from wrapper import solve


def count_trees(map_, right, down):
    result = 0
    height, width = len(map_), len(map_[0])

    for y, i in zip(range(down, height, down), itertools.count(start=1)):
        x = i * right % width

        if map_[y][x] == "#":
            result += 1

    print("Right {}, down {}: {}".format(right, down, result))
    return result


def solution(inp):
    map_ = inp.strip().split("\n")

    return reduce(
        int.__mul__,
        (
            count_trees(map_, right=1, down=1),
            count_trees(map_, right=3, down=1),
            count_trees(map_, right=5, down=1),
            count_trees(map_, right=7, down=1),
            count_trees(map_, right=1, down=2),
        ),
    )


if __name__ == "__main__":
    solve(solution, day=3, level=2)
