from wrapper import solve
import itertools


def solution(input_):
    inp = map(int, input_.strip().split("\n"))
    preamble = [next(inp) for _ in range(25)]
    n, npos = None, None

    for i, x in enumerate(inp, start=25):
        sums = set(a + b for a, b in itertools.combinations(preamble, 2))

        if x not in sums:
            n, npos = x, i
            break

        preamble.pop(0)
        preamble.append(x)

    inp = list(map(int, input_.strip().split("\n")))

    for win_size in range(3, npos):
        for shift in range(npos - win_size):
            sample = inp[shift : shift + win_size]

            if sum(sample) == n:
                return min(sample) + max(sample)


if __name__ == "__main__":
    solve(solution, day=9, level=2)
