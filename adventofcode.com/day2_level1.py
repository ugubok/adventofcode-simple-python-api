from wrapper import solve
import itertools


def solution(inp):
    lines = inp.strip().split("\n")
    result = 0
    for entry in lines:
        entry = entry.replace(":", "").strip()
        range_, letter, password = entry.split(" ")
        from_, to = map(int, range_.split("-"))

        if password.count(letter) in range(from_, to + 1):
            result += 1

    return result


solve(solution, day=2, level=1)
