from wrapper import solve
import itertools


def solution(inp):
    lines = inp.strip().split("\n")
    result = 0

    for entry in lines:
        entry = entry.replace(":", "").strip()
        range_, letter, password = entry.split(" ")
        x, y = map(int, range_.split("-"))

        is_valid = (password[x - 1] == letter) ^ (password[y - 1] == letter)

        if is_valid:
            result += 1

        prefix = "✅" if is_valid else "❌"
        print("{} {}-{} {}: {}".format(prefix, x, y, letter, password))

    return result


solve(solution, day=2, level=2)
